package com.eightam.cleanupscheduling;

public interface CleanupTarget {

    void clean() throws CleanupException;

}
