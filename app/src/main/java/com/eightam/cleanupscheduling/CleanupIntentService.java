package com.eightam.cleanupscheduling;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import java.io.File;

import static com.eightam.cleanupscheduling.Broadcasts.ACTION_CLEAN_UP_END;
import static com.eightam.cleanupscheduling.Broadcasts.ACTION_CLEAN_UP_START;
import static com.eightam.cleanupscheduling.ServiceLocator.get;

public class CleanupIntentService extends IntentService {

    public CleanupIntentService() {
        super(CleanupIntentService.class.getName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        CleanupManager cleanupManager = get(CleanupManager.class);
        DirectoryLocator directoryLocator = get(DirectoryLocator.class);
        LocalBroadcastManager broadcastManager = get(LocalBroadcastManager.class);

        broadcastManager.sendBroadcast(new Intent(ACTION_CLEAN_UP_START));
        File[] files = directoryLocator.getFilesDir().listFiles();

        for (File file : files) {
            cleanupManager.process(new FileTarget(file));
        }

        broadcastManager.sendBroadcast(new Intent(ACTION_CLEAN_UP_END));
        cleanupManager.scheduleNextCleanup();
    }

}
