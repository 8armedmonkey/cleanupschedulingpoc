package com.eightam.cleanupscheduling;

public class AndRule implements CleanupRule {

    private final CleanupRule rule1;
    private final CleanupRule rule2;

    public AndRule(CleanupRule rule1, CleanupRule rule2) {
        this.rule1 = rule1;
        this.rule2 = rule2;
    }

    @Override
    public boolean match(CleanupTarget target) {
        return rule1.match(target) && rule2.match(target);
    }

}
