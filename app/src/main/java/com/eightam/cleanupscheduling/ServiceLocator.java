package com.eightam.cleanupscheduling;

import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.synchronizedMap;

public final class ServiceLocator {

    private static Map<String, Object> registry = synchronizedMap(new HashMap<String, Object>());

    private ServiceLocator() {
    }

    public static <T> void register(Class<T> clazz, T instance) {
        registry.put(clazz.getName(), instance);
    }

    @SuppressWarnings("unused")
    public static void unregister(Class<?> clazz) {
        registry.remove(clazz.getName());
    }

    public static <T> T get(Class<T> clazz) {
        return clazz.cast(registry.get(clazz.getName()));
    }

}
