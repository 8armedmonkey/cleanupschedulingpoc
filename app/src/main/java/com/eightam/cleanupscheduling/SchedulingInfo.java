package com.eightam.cleanupscheduling;

public final class SchedulingInfo {

    private final boolean needed;
    private final long when;

    public SchedulingInfo(boolean needed, long when) {
        this.needed = needed;
        this.when = when;
    }

    public boolean isNeeded() {
        return needed;
    }

    public long getWhen() {
        return when;
    }

}
