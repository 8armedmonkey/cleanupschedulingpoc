package com.eightam.cleanupscheduling;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import static android.app.PendingIntent.FLAG_CANCEL_CURRENT;
import static android.app.PendingIntent.FLAG_NO_CREATE;
import static android.app.PendingIntent.getService;
import static java.util.Objects.requireNonNull;

public class CleanupSchedulerImpl implements CleanupScheduler {

    private Context appContext;

    public CleanupSchedulerImpl(Context context) {
        appContext = context.getApplicationContext();
    }

    @Override
    public void schedule(long timeInFutureMillis) {
        AlarmManager am = (AlarmManager) appContext.getSystemService(Context.ALARM_SERVICE);

        requireNonNull(am).set(
            AlarmManager.RTC_WAKEUP,
            timeInFutureMillis,
            createPendingIntent(appContext)
        );
    }

    @Override
    public void cancel() {
        AlarmManager am = (AlarmManager) appContext.getSystemService(Context.ALARM_SERVICE);
        PendingIntent pendingIntent = createPendingIntent(appContext);

        requireNonNull(am).cancel(pendingIntent);
        pendingIntent.cancel();
    }

    @Override
    public boolean hasNextCleanup() {
        return pendingIntentExists(appContext);
    }

    @Override
    public long nextCleanupTimeMillis() {
        throw new UnsupportedOperationException();
    }

    private static PendingIntent createPendingIntent(Context appContext) {
        return createPendingIntent(appContext, FLAG_CANCEL_CURRENT);
    }

    private static PendingIntent createPendingIntent(Context appContext, int flag) {
        return getService(appContext, 0, new Intent(appContext, CleanupIntentService.class), flag);
    }

    private static boolean pendingIntentExists(Context appContext) {
        return createPendingIntent(appContext, FLAG_NO_CREATE) != null;
    }

}
