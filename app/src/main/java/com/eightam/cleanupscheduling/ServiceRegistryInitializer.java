package com.eightam.cleanupscheduling;

import android.content.Context;
import android.support.v4.content.LocalBroadcastManager;

import java.io.File;

import static com.eightam.cleanupscheduling.ServiceLocator.register;

public final class ServiceRegistryInitializer {

    public static void init(Context context) {
        DirectoryLocator directoryLocator = newDirectoryLocator(context);
        FileCreator fileCreator = newFileCreator();
        CleanupManager cleanupManager = newCleanupManager(context, directoryLocator.getFilesDir());
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);

        register(DirectoryLocator.class, directoryLocator);
        register(FileCreator.class, fileCreator);
        register(CleanupManager.class, cleanupManager);
        register(LocalBroadcastManager.class, broadcastManager);
    }

    private static DirectoryLocator newDirectoryLocator(Context context) {
        return new DirectoryLocatorImpl(context);
    }

    private static FileCreator newFileCreator() {
        return new FileCreatorImpl();
    }

    private static CleanupManager newCleanupManager(Context context, File directory) {
        CleanupManager cleanupManager = new CleanupManagerImpl();

        cleanupManager.addRule(
            new AndRule(
                new FileMaxAgeCleanupRule(Constants.FILE_MAX_AGE_MILLIS),
                new FileNamePrefixRule(Constants.FILE_PREFIX)
            )
        );

        cleanupManager.setScheduler(new CleanupSchedulerImpl(context));
        cleanupManager.setSchedulingRule(new FileMaxAgeSchedulingRule(
            directory,
            Constants.FILE_MAX_AGE_MILLIS
        ));

        return cleanupManager;
    }

    private ServiceRegistryInitializer() {
    }

}
