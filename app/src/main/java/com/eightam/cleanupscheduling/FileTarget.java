package com.eightam.cleanupscheduling;

import java.io.File;

public class FileTarget implements CleanupTarget {

    private File file;

    public FileTarget(File file) {
        this.file = file;
    }

    @Override
    public void clean() throws CleanupException {
        if (!file.delete()) {
            throw new CleanupException();
        }
    }

    public File getFile() {
        return file;
    }

}
