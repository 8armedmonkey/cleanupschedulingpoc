package com.eightam.cleanupscheduling;

public interface CleanupManager {

    CleanupRules getRules();

    void addRule(CleanupRule rule);

    void removeRule(CleanupRule rule);

    void process(CleanupTarget target);

    void setScheduler(CleanupScheduler scheduler);

    void setSchedulingRule(SchedulingRule schedulingRule);

    void unsetSchedulingRule();

    void scheduleNextCleanup();

    void cancelNextCleanup();

    boolean hasNextCleanup();

    long nextCleanupTimeMillis();

}