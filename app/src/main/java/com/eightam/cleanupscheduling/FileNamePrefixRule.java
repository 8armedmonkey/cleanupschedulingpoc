package com.eightam.cleanupscheduling;

public class FileNamePrefixRule implements CleanupRule {

    private final String prefix;

    public FileNamePrefixRule(String prefix) {
        this.prefix = prefix;
    }

    @Override
    public boolean match(CleanupTarget target) {
        return target instanceof FileTarget
            && ((FileTarget) target).getFile().getName().startsWith(prefix);
    }

}
