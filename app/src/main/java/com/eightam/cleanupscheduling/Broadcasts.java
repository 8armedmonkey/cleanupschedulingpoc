package com.eightam.cleanupscheduling;

public final class Broadcasts {

    public static final String ACTION_NEW_FILE = "broadcasts.ACTION_NEW_FILE";
    public static final String ACTION_CLEAN_UP_START = "broadcasts.ACTION_CLEAN_UP_START";
    public static final String ACTION_CLEAN_UP_END = "broadcasts.ACTION_CLEAN_UP_END";

    private Broadcasts() {
    }

}
