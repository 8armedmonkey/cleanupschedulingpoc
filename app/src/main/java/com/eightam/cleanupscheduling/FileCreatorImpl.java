package com.eightam.cleanupscheduling;

import java.io.File;
import java.io.IOException;

import static com.eightam.cleanupscheduling.FileUtils.newFileName;

public class FileCreatorImpl implements FileCreator {

    @Override
    public void newFile(File directory) throws IOException {
        File newFile = new File(directory, newFileName());

        if (!newFile.createNewFile()) {
            throw new IOException();
        }
    }

}
