package com.eightam.cleanupscheduling;

import static java.lang.System.currentTimeMillis;

public class FileMaxAgeCleanupRule implements CleanupRule {

    private final long maxAgeMillis;

    public FileMaxAgeCleanupRule(long maxAgeMillis) {
        this.maxAgeMillis = maxAgeMillis;
    }

    @Override
    public boolean match(CleanupTarget target) {
        return maxAgeMillis > 0
            && target instanceof FileTarget
            && (currentTimeMillis() - ((FileTarget) target).getFile().lastModified()) >= maxAgeMillis;
    }

}
