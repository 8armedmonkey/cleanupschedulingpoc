package com.eightam.cleanupscheduling;

public interface SchedulingRule {

    SchedulingInfo getInfo();

}
