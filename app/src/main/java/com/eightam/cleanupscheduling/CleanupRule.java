package com.eightam.cleanupscheduling;

public interface CleanupRule {

    boolean match(CleanupTarget target);

}
