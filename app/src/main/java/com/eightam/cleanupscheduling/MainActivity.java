package com.eightam.cleanupscheduling;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.io.File;
import java.io.IOException;

import static android.support.v7.widget.LinearLayoutManager.VERTICAL;
import static android.widget.Toast.LENGTH_SHORT;
import static android.widget.Toast.makeText;
import static com.eightam.cleanupscheduling.Broadcasts.ACTION_CLEAN_UP_END;
import static com.eightam.cleanupscheduling.Broadcasts.ACTION_CLEAN_UP_START;
import static com.eightam.cleanupscheduling.Broadcasts.ACTION_NEW_FILE;
import static com.eightam.cleanupscheduling.FileUtils.orderByLastModifiedDesc;
import static com.eightam.cleanupscheduling.ServiceLocator.get;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private BroadcastReceiver receiver;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonCreateNewFile:
                createNewFile();
                break;
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initBroadcastReceiver();
        initViews();
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerBroadcastReceiver();
    }

    @Override
    protected void onStop() {
        unregisterBroadcastReceiver();
        super.onStop();
    }

    private void initBroadcastReceiver() {
        receiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                if (ACTION_NEW_FILE.equals(intent.getAction())) {
                    updateFiles();

                } else if (ACTION_CLEAN_UP_START.equals(intent.getAction())) {
                    showCleanupStarted();

                } else if (ACTION_CLEAN_UP_END.equals(intent.getAction())) {
                    showCleanupFinished();
                    updateFiles();

                }
            }

        };
    }

    private void registerBroadcastReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_NEW_FILE);
        filter.addAction(ACTION_CLEAN_UP_START);
        filter.addAction(ACTION_CLEAN_UP_END);

        get(LocalBroadcastManager.class).registerReceiver(receiver, filter);
    }

    private void unregisterBroadcastReceiver() {
        get(LocalBroadcastManager.class).unregisterReceiver(receiver);
    }

    private void initViews() {
        findViewById(R.id.buttonCreateNewFile).setOnClickListener(this);

        ((RecyclerView) findViewById(R.id.recyclerFiles)).setLayoutManager(
            new LinearLayoutManager(this, VERTICAL, false)
        );

        updateFiles();
    }

    private void createNewFile() {
        try {
            File directory = get(DirectoryLocator.class).getFilesDir();
            get(FileCreator.class).newFile(directory);

            get(LocalBroadcastManager.class).sendBroadcast(new Intent(ACTION_NEW_FILE));
            get(CleanupManager.class).scheduleNextCleanup();

        } catch (IOException e) {
            showError(e);
        }
    }

    private void showCleanupStarted() {
        makeText(getApplicationContext(), R.string.cleanup_start_message, LENGTH_SHORT).show();
    }

    private void showCleanupFinished() {
        makeText(getApplicationContext(), R.string.cleanup_finish_message, LENGTH_SHORT).show();
    }

    private void updateFiles() {
        ((RecyclerView) findViewById(R.id.recyclerFiles)).setAdapter(
            new FileAdapter(
                orderByLastModifiedDesc(
                    get(DirectoryLocator.class).getFilesDir().listFiles()
                )
            )
        );
    }

    private void showError(Throwable t) {
        makeText(getApplicationContext(), t.getMessage(), LENGTH_SHORT).show();
    }

}
