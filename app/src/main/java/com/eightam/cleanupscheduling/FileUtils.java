package com.eightam.cleanupscheduling;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;

import static java.lang.System.currentTimeMillis;

public final class FileUtils {

    public static File findOldest(File[] files) {
        assertArrayNotEmpty(files);

        File oldest = files[0];

        for (int i = 1; i < files.length; i++) {
            if (oldest.lastModified() > files[i].lastModified()) {
                oldest = files[i];
            }
        }
        return oldest;
    }

    public static File[] orderByLastModifiedDesc(File[] files) {
        File[] sortedFiles = Arrays.copyOf(files, files.length);

        Arrays.sort(sortedFiles, new Comparator<File>() {
            @Override
            public int compare(File o1, File o2) {
                return -1 * Long.compare(o1.lastModified(), o2.lastModified());
            }
        });

        return sortedFiles;
    }

    public static String newFileName() {
        return "foo-" + currentTimeMillis() + ".txt";
    }

    private static void assertArrayNotEmpty(File[] files) {
        if (files == null || files.length == 0) {
            throw new IllegalArgumentException();
        }
    }

    private FileUtils() {
    }

}
