package com.eightam.cleanupscheduling;

import java.io.File;
import java.io.IOException;

public interface FileCreator {

    void newFile(File directory) throws IOException;

}
