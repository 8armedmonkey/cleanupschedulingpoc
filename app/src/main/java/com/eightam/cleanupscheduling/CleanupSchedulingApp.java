package com.eightam.cleanupscheduling;

import android.app.Application;
import android.content.Intent;

public class CleanupSchedulingApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        initServiceRegistry();
        initCleanup();
    }

    private void initServiceRegistry() {
        ServiceRegistryInitializer.init(this);
    }

    private void initCleanup() {
        startService(new Intent(this, CleanupIntentService.class));
    }

}
