package com.eightam.cleanupscheduling;

import java.io.File;

public interface DirectoryLocator {

    File getFilesDir();

}
