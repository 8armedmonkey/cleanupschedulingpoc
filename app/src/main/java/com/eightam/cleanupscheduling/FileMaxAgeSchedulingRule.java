package com.eightam.cleanupscheduling;

import java.io.File;

import static java.lang.System.currentTimeMillis;

public class FileMaxAgeSchedulingRule implements SchedulingRule {

    private final File directory;
    private final long maxAgeMillis;

    public FileMaxAgeSchedulingRule(File directory, long maxAgeMillis) {
        this.directory = directory;
        this.maxAgeMillis = maxAgeMillis;
    }

    @Override
    public SchedulingInfo getInfo() {
        File[] files = directory.listFiles();

        if (maxAgeMillis <= 0 || files.length == 0) {
            return new SchedulingInfo(false, 0);
        } else {
            File oldestFile = FileUtils.findOldest(files);
            long currentTimeMillis = currentTimeMillis();
            long oldestFileExpiryTimeMillis = oldestFile.lastModified() + maxAgeMillis;

            if (oldestFileExpiryTimeMillis < currentTimeMillis) {
                return new SchedulingInfo(true, currentTimeMillis + maxAgeMillis);
            } else {
                return new SchedulingInfo(true, oldestFileExpiryTimeMillis);
            }
        }
    }

}
