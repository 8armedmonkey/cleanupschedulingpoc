package com.eightam.cleanupscheduling;

public final class Constants {

    public static final long FILE_MAX_AGE_MILLIS = 30000;

    public static final String FILE_PREFIX = "foo-";

    private Constants() {
    }

}
