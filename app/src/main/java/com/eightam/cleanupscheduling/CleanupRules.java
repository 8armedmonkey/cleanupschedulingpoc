package com.eightam.cleanupscheduling;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class CleanupRules implements Iterable<CleanupRule> {

    private List<CleanupRule> rules;

    public CleanupRules(CleanupRule... rules) {
        this.rules = new ArrayList<>();

        if (rules != null) {
            this.rules.addAll(Arrays.asList(rules));
        }
    }

    @NonNull
    @Override
    public Iterator<CleanupRule> iterator() {
        return rules.iterator();
    }

}
