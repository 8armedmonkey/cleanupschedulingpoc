package com.eightam.cleanupscheduling;

public interface CleanupScheduler {

    void schedule(long timeInFutureMillis);

    void cancel();

    boolean hasNextCleanup();

    long nextCleanupTimeMillis();

}
