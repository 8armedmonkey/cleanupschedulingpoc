package com.eightam.cleanupscheduling;

import android.content.Context;

import java.io.File;

public class DirectoryLocatorImpl implements DirectoryLocator {

    private Context appContext;

    public DirectoryLocatorImpl(Context context) {
        appContext = context.getApplicationContext();
    }

    @Override
    public File getFilesDir() {
        return appContext.getFilesDir();
    }

}
