package com.eightam.cleanupscheduling;

import java.util.LinkedHashSet;
import java.util.Set;

public class CleanupManagerImpl implements CleanupManager {

    private Set<CleanupRule> rules;
    private CleanupScheduler scheduler;
    private SchedulingRule schedulingRule;

    public CleanupManagerImpl() {
        rules = new LinkedHashSet<>();
    }

    @Override
    public synchronized CleanupRules getRules() {
        return new CleanupRules(rules.toArray(new CleanupRule[rules.size()]));
    }

    @Override
    public synchronized void addRule(CleanupRule rule) {
        rules.add(rule);
    }

    @Override
    public synchronized void removeRule(CleanupRule rule) {
        rules.remove(rule);
    }

    @Override
    public synchronized void process(CleanupTarget target) {
        for (CleanupRule rule : rules) {
            if (rule.match(target)) {
                target.clean();
                break;
            }
        }
    }

    @Override
    public synchronized void setScheduler(CleanupScheduler scheduler) {
        this.scheduler = scheduler;
    }

    @Override
    public synchronized void setSchedulingRule(SchedulingRule schedulingRule) {
        this.schedulingRule = schedulingRule;
    }

    @Override
    public synchronized void unsetSchedulingRule() {
        this.schedulingRule = null;
    }

    @Override
    public synchronized void scheduleNextCleanup() {
        if (schedulingRule != null && scheduler != null) {
            SchedulingInfo info = schedulingRule.getInfo();

            if (info != null && info.isNeeded()) {
                scheduler.schedule(info.getWhen());
            } else {
                scheduler.cancel();
            }
        }
    }

    @Override
    public synchronized void cancelNextCleanup() {
        if (scheduler != null) {
            scheduler.cancel();
        }
    }

    @Override
    public synchronized boolean hasNextCleanup() {
        return scheduler != null && scheduler.hasNextCleanup();
    }

    @Override
    public synchronized long nextCleanupTimeMillis() {
        if (scheduler != null) {
            return scheduler.nextCleanupTimeMillis();
        } else {
            return 0;
        }
    }

}
